package com.example.retrofitpost

import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.login.view.*

internal fun MainActivity.showUsernameDialog(callback: () -> Unit) {

    var dialogBuilder = AlertDialog.Builder(this)
    val dialogView = layoutInflater.inflate(R.layout.login, null)

    dialogBuilder.setView(dialogView)
    dialogBuilder.setTitle("GitHub Username")
    dialogBuilder.setPositiveButton("Ok") { _, _ ->
        AuthenticationPrefs.saveUserName(dialogView.username.text.toString())
        callback()
    }
    dialogBuilder.setNegativeButton("Cancel") { _, _ ->

    }

    val dialog = dialogBuilder.create()
    dialog.setOnShowListener {
//        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
    }

    dialogView.username?.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled =  !dialogView.username.text.isNullOrBlank()
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    })

    dialog.show()

}