package com.example.retrofitpost

import android.app.Application
import android.content.Context
/*
Add code to Android Manifest
<application
android:name=".RetrofitPostApplication"
*/
class RetrofitPostApplication: Application() {

    companion object {
        private lateinit var instance: RetrofitPostApplication
        var appContext : Context? = null
            get() = instance.applicationContext

    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

}