package com.example.retrofitpost.model

import com.google.gson.annotations.SerializedName

data class Gist(@SerializedName("created_at") var createdAt: String,
                val description: String,
                val files: Map<String, GistFile>,
                val id: String)