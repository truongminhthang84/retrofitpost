package com.example.retrofitpost.model

data class GistRequest (val description: String, val files: Map<String, GistFile>, val public: Boolean = true)

