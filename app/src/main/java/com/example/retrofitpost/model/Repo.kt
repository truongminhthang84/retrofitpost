package com.example.retrofitpost.model


import com.google.gson.annotations.SerializedName

data class Repo (@SerializedName("name") var name: String)