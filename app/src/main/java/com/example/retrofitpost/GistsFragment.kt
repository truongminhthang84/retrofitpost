package com.example.retrofitpost

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofitpost.api.Repository
import com.example.retrofitpost.model.EmptyResponse
import com.example.retrofitpost.model.Gist
import com.example.retrofitpost.model.GistFile
import com.example.retrofitpost.model.GistRequest
import kotlinx.android.synthetic.main.cell.view.*
import kotlinx.android.synthetic.main.dialog_gist.view.*
import kotlinx.android.synthetic.main.fragment_gists.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GistsFragment : Fragment() {
    private val gists = arrayListOf<Gist>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gists, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        floatingActionButton.setOnClickListener {
            showGistDialog()
        }
    }

    private fun showGistDialog() {
        var dialogBuilder = AlertDialog.Builder(context!!)
        val dialogView = layoutInflater.inflate(R.layout.dialog_gist, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setTitle("GitHub Username")
        dialogBuilder.setPositiveButton("Ok") { _, _ ->
            val filename = dialogView.filenameTextview.text.toString()
            val description = dialogView.descriptionTextView.text.toString()
            val content = dialogView.contentTextview.text.toString()
            postGist(description, filename, content)

        }
        dialogBuilder.setNegativeButton("Cancel") { _, _ ->

        }

        val dialog = dialogBuilder.create()
        dialog.setOnShowListener {
            //        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
        }


        dialog.show()
    }

    private fun setupRecyclerView() {
        gistsRecyclerView.layoutManager = LinearLayoutManager(context)
        gistsRecyclerView.adapter = GistsAdapter()
    }

    inner class GistsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.cell, parent, false)

            val viewHolder = GistsViewHolder(itemView)
            itemView.setOnClickListener {
                val selectedIndex = viewHolder.adapterPosition
                val gist = gists[selectedIndex]
                this@GistsFragment.deleteGist(gist, selectedIndex)
            }
            return viewHolder
        }

        override fun getItemCount(): Int = gists.size

        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
            if (viewHolder is GistsViewHolder) {
                viewHolder.itemView.textView.text = gists[position].description
            }
        }
    }

    inner class GistsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onResume() {
        super.onResume()
        loadData()
    }

    fun loadData() {
        if (AuthenticationPrefs.isAuthenticated()) {
            var username = AuthenticationPrefs.getUsername()
            Repository.githubApi.getGists(username).enqueue(object : Callback<ArrayList<Gist>> {
                override fun onFailure(call: Call<ArrayList<Gist>>, t: Throwable) {

                }

                override fun onResponse(call: Call<ArrayList<Gist>>, response: Response<ArrayList<Gist>>) {
                    if (response.body() != null) {
                        gists.clear()
                        gists.addAll(response.body()!!)
                        gistsRecyclerView.adapter?.notifyDataSetChanged()
                    }
                }
            })
        }
    }

    private fun postGist(description: String, filename: String, content: String) {
        val gistFile = GistFile(content)
        val request = GistRequest(description, mapOf(filename to gistFile))
        Repository.githubApi.postGist(request).enqueue(object : Callback<Gist> {
            override fun onFailure(call: Call<Gist>, t: Throwable) {
                println("Sending gist fail")

            }

            override fun onResponse(call: Call<Gist>, response: Response<Gist>) {
                if (response.body() != null) {
                    gists.add(0, response.body()!!)
                    gistsRecyclerView.adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun deleteGist(gist: Gist, index: Int) {
        Repository.githubApi.deleteGist(gist.id).enqueue(object : Callback<EmptyResponse> {
            override fun onFailure(call: Call<EmptyResponse>, t: Throwable) {

            }

            override fun onResponse(call: Call<EmptyResponse>, response: Response<EmptyResponse>) {
                gists.remove(gist)
                gistsRecyclerView.adapter?.notifyItemRemoved(index)
            }
        })
    }


}
