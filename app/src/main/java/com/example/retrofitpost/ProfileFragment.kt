package com.example.retrofitpost

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.retrofitpost.api.Repository
import com.example.retrofitpost.model.Repo
import com.example.retrofitpost.model.User
import com.example.retrofitpost.model.UserRequest
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment: Fragment() {

     private var user : User? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
         update_button.setOnClickListener {
            updateUser()
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    override fun onPause() {
        super.onPause()
    }

    fun loadData() {
        Repository.githubApi.getUser(AuthenticationPrefs.getUsername()).enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {

            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.body() != null) {
                    user = response.body()
                    updateUI()
                }
            }
        })
    }

    private fun updateUI() {
        login_textview.text = user?.login
        company.text = user?.company
        repo_name.text = user?.name
        Picasso.with(context).load(user?.avatarUrl).into(avatar)
    }


    private fun updateUser() {
        val companyName = company_edit_text.text.toString()
        Repository.githubApi.updateUser(UserRequest(companyName)).enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {

            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.body() != null) {
                    user = response.body()
                    updateUI()
                }
            }
        })
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }


}