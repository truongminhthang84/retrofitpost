package com.example.retrofitpost.api

import com.example.retrofitpost.model.AccessToken
import retrofit2.Call
import retrofit2.http.*

interface AuthAPI {
    @Headers("Accept: application/json")
    @POST("login/oauth/access_token")
    @FormUrlEncoded
    fun getAccessToken(@Field("client_id") clientID: String,
                       @Field("client_secret") clientSecret: String,
                       @Field("code") accessCode: String
                       ): Call<AccessToken>
}