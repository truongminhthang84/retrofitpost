package com.example.retrofitpost.api

import com.example.retrofitpost.AuthenticationPrefs
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Repository {

     val githubApi  : GitHubApi by lazy {
         Retrofit.Builder()
                 .baseUrl("https://api.github.com/")
                 .addConverterFactory(GsonConverterFactory.create())
                 .client(okHttpClient)
                 .build()
                 .create(GitHubApi::class.java)
     }
    val api : AuthAPI  =
            Retrofit.Builder()
                    .baseUrl("https://github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(AuthAPI::class.java)

    private val okHttpClient : OkHttpClient = OkHttpClient.Builder()
            .addInterceptor {
                val request = it.request().newBuilder().addHeader("Authorization", "token ${AuthenticationPrefs.getAuthToken()}").build()
                it.proceed(request)
            }
            .build()
}