package com.example.retrofitpost

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.retrofitpost.api.Repository
import com.example.retrofitpost.model.AccessToken
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {




    private val repoFragment = RepoFragment()
    private val gistsFragment = GistsFragment()
    private val profileFragment = ProfileFragment()
    lateinit var fragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        bottomNavigationView.setOnNavigationItemSelectedListener {
            fragment = when (it.itemId) {
                R.id.repos -> repoFragment
                R.id.gists -> gistsFragment
                else ->  profileFragment

            }
            switchToFragment(fragment)
            true
        }
        bottomNavigationView.selectedItemId = R.id.repos

    }


    override fun onResume() {
        super.onResume()
        val uri = intent.data
        if (uri != null && uri.toString().startsWith(BuildConfig.REDIRECT_URI)) {
            val accessCode = uri.getQueryParameter("code")

            Repository.api.getAccessToken(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, accessCode).enqueue(object : Callback<AccessToken> {
                override fun onFailure(call: Call<AccessToken>, t: Throwable) {
                    Log.e("MainActivity", "fail to get access token")
                }

                override fun onResponse(call: Call<AccessToken>, response: Response<AccessToken>) {
                    val accessToken = response.body()?.accessToken
                    val tokenType = response.body()?.tokenType
                    if (accessToken != null && tokenType != null) {
                        AuthenticationPrefs.saveAuthToken(accessToken)
                        AuthenticationPrefs.saveTokenType(tokenType)
                    }
                    switchToFragment(gistsFragment)
                    gistsFragment.loadData()
                }
            })

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.loginItem -> login()
            R.id.logoutItem -> logout()
        }
        return true
    }

    private fun logout() {
        AuthenticationPrefs.logout()
    }

    private fun login() {
        if (!AuthenticationPrefs.isAuthenticated()) {
            showUsernameDialog {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(
                        "https://github.com/login/oauth/authorize?client_id=${BuildConfig.CLIENT_ID}&scope=user%20gist&redirect_uri=${BuildConfig.REDIRECT_URI}"))
                startActivity(intent)
            }
        }
    }

    private fun switchToFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fragment).commit()
    }

}



