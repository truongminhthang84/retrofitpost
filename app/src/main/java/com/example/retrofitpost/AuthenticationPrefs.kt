package com.example.retrofitpost

import android.preference.PreferenceManager

object AuthenticationPrefs {

    private const val KEY_AUTH_TOKEN = "KEY_AUTH_TOKEN"

    private const val KEY_TOKEN_TYPE = "KEY_TOKEN_TYPE"

    private const val KEY_USERNAME = "KEY_USERNAME"

    private var sharedPrefs = PreferenceManager.getDefaultSharedPreferences(RetrofitPostApplication.appContext)

    fun saveAuthToken(token: String) {
        val editor = sharedPrefs.edit()
        editor.putString(KEY_AUTH_TOKEN, token).apply()
    }

    fun getAuthToken() = sharedPrefs.getString(KEY_AUTH_TOKEN, "")

    fun isAuthenticated(): Boolean {
        val x = getAuthToken()
        return !x.isBlank()

    }

    fun saveTokenType(tokenType: String) {
        val editor = sharedPrefs.edit()
        editor.putString(KEY_TOKEN_TYPE, tokenType).apply()
    }

    fun getTokenType(): String = sharedPrefs.getString(KEY_TOKEN_TYPE, "")

    fun saveUserName(username: String) {
        val editor = sharedPrefs.edit()
        editor.putString(KEY_USERNAME, username).apply()
    }

    fun getUsername(): String = sharedPrefs.getString(KEY_USERNAME, "")

    fun clearUsername() = sharedPrefs.edit().remove(KEY_USERNAME).apply()

    fun clearAuthToken() = saveAuthToken("")

    fun logout() {
        clearAuthToken()
        clearUsername()
    }
}