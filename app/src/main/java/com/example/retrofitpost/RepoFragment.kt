package com.example.retrofitpost

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofitpost.api.GitHubApi
import com.example.retrofitpost.api.Repository
import com.example.retrofitpost.model.Gist
import com.example.retrofitpost.model.GistRequest
import com.example.retrofitpost.model.Repo
import kotlinx.android.synthetic.main.cell.view.*
import kotlinx.android.synthetic.main.fragment_repo.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RepoFragment : Fragment() {
    private val repos = arrayListOf<Repo>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_repo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = ReposAdapter()
    }
    inner class ReposAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.cell, parent, false)
            return RepoViewHolder(itemView)
        }

        override fun getItemCount(): Int = repos.size

        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
            if (viewHolder is RepoViewHolder){
                viewHolder.itemView.textView.text = repos[position].name
            }
        }
    }

    inner class RepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onResume() {
        super.onResume()
       loadData()
    }

    fun loadData() {
        if (AuthenticationPrefs.isAuthenticated()) {
            var username = AuthenticationPrefs.getUsername()
            Repository.githubApi.getRepos(username).enqueue(object : Callback<ArrayList<Repo>> {
                override fun onFailure(call: Call<ArrayList<Repo>>, t: Throwable) {

                }

                override fun onResponse(call: Call<ArrayList<Repo>>, response: Response<ArrayList<Repo>>) {
                    if (response.body() != null) {
                        repos.clear()
                        repos.addAll(response.body()!!)
                        recyclerView.adapter?.notifyDataSetChanged()
                    }
                }
            })
        }
    }



}
